var express = require('express');
const calc = require('./calc.js');
var parser = require('body-parser');
var path = require('path');
let {PythonShell} = require('python-shell');
var app = express();
var awsCli = require('aws-cli-js');
var Options = awsCli.Options;
var Aws = awsCli.Aws;
var options = new Options(
    /* accessKey    */ 'your key',
    /* secretKey    */ 'your key2',
    /* sessionToken */ 'your token',
    /* currentWorkingDirectory */ null
  );
var aws = new Aws(options);
app.use(parser.urlencoded({ extended: false }))
app.use(parser.json())
app.use(function(req,res,next){
    res.locals.userValue = null;
    next();
})
 
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'app_views'))
 
app.get('/',function(req,res){
    res.render('home',{
        clusterValue : 'ClusterValue',
        listInstances : 'listInstances',
        listServices: 'listServices',
        listServicesosl: "listServicesosl",
        runNodeShell: "runNodeShell",
        listServices2: "listServices2",
        listServices3: "listServices3",
        runNodeShellosl: "runNodeShellosl"
    });
    console.log('user accessing Home page');
});
app.post('/list-clusters', function (req, res) {
  var prof = req.body.profilename.split(',')[0]
  var regionname = req.body.profilename.split(',')[1]
  var options = {
    mode: 'text',
    pythonPath: '/usr/bin/python', 
    pythonOptions: ['-v'],
    args: [ 'list_clusters()', '' + prof + '', '' + regionname + ''
   ]
  }
  PythonShell.run('./boto.py', options, function (err, data) {
    if (err) res.send(err);
    res.render("home1", { clusterValue: JSON.stringify(data,null,'\t'), listServices: "listServices", listServicesosl: "listServicesosl", listInstances: "listInstances", runNodeShell: "runNodeShell", runNodeShellosl: "runNodeShellosl",listServices2 : "listServices2", listServices3 : "listServices3"} );
  });
    });

app.post('/list-services', function (req, res) {
  var clustername = req.body.listclusterselect
  var prof2 = req.body.profilenamesl.split(',')[0]
  var regionname = req.body.profilenamesl.split(',')[1]
  var options = {
    mode: 'text',
    pythonPath: '/usr/bin/python', 
    pythonOptions: ['-v'],
    args: [ 'list_services()', '' + prof2 + '', '' + regionname + '', '' + clustername + '' 
   ]
  }
    PythonShell.run('./boto.py', options, function (err, data) {
    if (err) res.send(err);
    res.render("home1", { clusterValue: "clusterValue", listServices: JSON.stringify(data,null,'\t'), listServicesosl: "listServicesosl", listInstances: "listInstances", runNodeShell: "runNodeShell",runNodeShellosl: "runNodeShellosl", listServices2 : "listServices2", listServices3 : "listServices3"});
  });
});
app.post('/list-servicesosl', function (req, res) {
  var clustername = req.body.clusterinstosl
  var prof2 = req.body.profilenamesosl.split(',')[0]
  var regionname = req.body.profilenamesosl.split(',')[1]
  var options = {
    mode: 'text',
    pythonPath: '/usr/bin/python', 
    pythonOptions: ['-v'],
    args: [ 'list_services()', '' + prof2 + '', '' + regionname + '', '' + clustername + '' 
   ]
  }
    PythonShell.run('./boto.py', options, function (err, data) {
    if (err) res.send(err);
    res.render("home1", { clusterValue: "clusterValue",runNodeShellosl: "runNodeShellosl", listServicesosl: JSON.stringify(data,null,'\t'), listServices: "listServices" , listInstances: "listInstances", runNodeShell: "runNodeShell", listServices2 : "listServices2", listServices3 : "listServices3"});
  });
});
app.post('/list-services2', function (req, res) {
  // 7.this is for listing IPS
  var clustername = req.body.listclusterselect2
  var prof2 = req.body.profilenamesl2.split(',')[0]
  var regionname = req.body.profilenamesl2.split(',')[1]
  var options = {
    mode: 'text',
    pythonPath: '/usr/bin/python', 
    pythonOptions: ['-v'],
    args: [ 'list_services()', '' + prof2 + '', '' + regionname + '', '' + clustername + '' 
   ]
  }
    PythonShell.run('./boto.py', options, function (err, data) {
    if (err) res.send(err);
    res.render("home1", { clusterValue: "clusterValue",listServicesosl: "listServicesosl",
     listServices: JSON.stringify(data,null,'\t'), listInstances: "listInstances", runNodeShell: "runNodeShell", runNodeShellosl: "runNodeShellosl", listServices2 : "listServices2", listServices3 : "listServices3"});
  });
});
    app.post('/list-instances', function (req, res) {
      var profilename = req.body.profilenameinst.split(',')[0]
      var regionname = req.body.profilenameinst.split(',')[1]
      var servicename = req.body.serviceinst
      var cluster = req.body.clusterinst
      var options = {
        mode: 'text',
        pythonPath: '/usr/bin/python', 
        args: [ 'list_container_instances()', '' + profilename + '', '' + regionname + '', '' + cluster + '', '' + servicename + ''
       ]
      }
      PythonShell.run('./boto.py', options, function (err, data) {
        if (err) res.send(err);
         res.render("home1", { clusterValue: "clusterValue", listServices: "listServices", listServicesosl: "listServicesosl", runNodeShell : "runNodeShell", runNodeShellosl: "runNodeShellosl",listInstances : JSON.stringify(data,null,'\t') + "\nservicename:" + servicename + "\nProfileName:" + profilename + "\n\nClusterName:" + cluster, listServices2 : "listServices2", listServices3 : "listServices3"});
      });
    });
    app.post('/list-instances2', function (req, res) {
      var profilename = req.body.profilenameinst2.split(',')[0]
      var regionname = req.body.profilenameinst2.split(',')[1]
      var servicename = req.body.serviceinst2
      var cluster = req.body.clusterinst2
      var options = {
        mode: 'text',
        pythonPath: '/usr/bin/python', 
        args: [ 'list_container_instances()', '' + profilename + '', '' + regionname + '', '' + cluster + '', '' + servicename + ''
       ]
      }
      PythonShell.run('./boto.py', options, function (err, data) {
        if (err) res.send(err);
         res.render("home", { clusterValue: "clusterValue", listServices: "listServices", listServicesosl: "listServicesosl",listInstances : JSON.stringify(data,null,'\t'), listServices2 : "listServices2", listServices3 : "listServices3"});
      });
    });
    app.post('/getclusters',  function (req, res) {
      // 4.this is for listing IPS
      var profilename = req.body.profilenamegl.split(',')[0]
      var regionname = req.body.profilenamegl.split(',')[1]
      data1 = [];
       aws.command(`ecs list-clusters --region ${regionname}  --profile  ${profilename}`).then(function (data) {
        data['object']['clusterArns'].toString().split(/\s*,\s*/).forEach(function(myString) {
          data1.push(myString.split('/')[1]);
      }); 
      setTimeout(function() {
        res.render("home1", { clusterValue: "clusterValue", listServices: "listServices", listServicesosl: "listServicesosl", listInstances :"listInstances",runNodeShellosl: "runNodeShellosl", runNodeShell : JSON.stringify(data1,null,'\t'), listServices2 : "listServices2", listServices3 : "listServices3"}); 
    }, 4000);
      
      });  
      });
      app.post('/getclustersosl',  function (req, res) {
        // 4.this is for listing IPS
        var profilename = req.body.profilenameglosl.split(',')[0]
        var regionname = req.body.profilenameglosl.split(',')[1]
        data1 = [];
         aws.command(`ecs list-clusters --region ${regionname}  --profile  ${profilename}`).then(function (data) {
          data['object']['clusterArns'].toString().split(/\s*,\s*/).forEach(function(myString) {
            data1.push(myString.split('/')[1]);
        }); 
        setTimeout(function() {
          res.render("home1", { clusterValue: "clusterValue", listServices: "listServices", listServicesosl: "listServicesosl", listInstances :"listInstances", runNodeShellosl : JSON.stringify(data1,null,'\t'), runNodeShell : JSON.stringify(data1,null,'\t'),  listServices2 : "listServices2", listServices3 : "listServices3"}); 
      }, 4000);
        
        });  
        });
app.listen(5000,function(){
    console.log('server running on port 5000');
})