#!/usr/local/bin/node
var awsCli = require('aws-cli-js');
var Options = awsCli.Options;
var Aws = awsCli.Aws;
var options = new Options(
    /* accessKey    */ 'your key',
    /* secretKey    */ 'your key2',
    /* sessionToken */ 'your token',
    /* currentWorkingDirectory */ null
  );
   
  var aws = new Aws(options);
   
  aws.command('ecs list-services --cluster ecs-prod-CA-tdp-cluster --region us-west-1 --profile tk-prod').then(function (data) {
    data['object']['serviceArns'].toString().split(/\s*,\s*/).forEach(function(myString) {
      console.log(myString.split('/')[1]);
  });  
  });
   
// var myJsRoutines = (function () {
//   var multiplier = 2;

//   return {
//     processLevel: function (level, callback) {
//       console.log('processLevel:', level); // CLI or /logs/express_output.log
//   aws.command('ecs list-clusters --region us-west-1 --profile tk-prod').then(function (data) {
//     data['object']['clusterArns'].toString().split(/\s*,\s*/).forEach(function(myString) {
//       console.log(myString.split('/')[1]);
//   });  
//   });
//       // validation
//       if (!level) {
//         // error is usually first param in node callback; null for success
//         callback('level is missing or 0');
//         return; // bail out
//       }

//       // processing
//       var result = level * multiplier;

//       // could return result, but need callback if code reads from file/db
//       callback(null, result);
//     }
//   };
// }()); // function executed so myJsRoutines is an object

// module.exports = myJsRoutines;